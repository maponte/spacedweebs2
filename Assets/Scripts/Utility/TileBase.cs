﻿using UnityEngine;
using System.Collections;

namespace BoogieDownGames {

	public class TileBase : MonoBehaviour {


		public virtual void Awake ()
		{

		}

		public virtual void Start ()
		{
			
		}

		public virtual void RunEndSequence()
		{

		}
	}
}