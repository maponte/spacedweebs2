﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace BoogieDownGames {

	public class CameraControls : MonoBehaviour {

		public Animator m_anime;
		public Button m_pauseButton;

		void Awake()
		{
			m_anime = GetComponent<Animator>();
		}

		public void SwitchToOrtho()
		{
			GetComponent<Camera>().orthographic = true;
		}

		public void SwitchToProjection()
		{
			GetComponent<Camera>().orthographic = false;
		}

		public void PlayAnime(string p_anime)
		{
			m_pauseButton.enabled = false;
			NotificationCenter.DefaultCenter.PostNotification(this, "hideButton");
			m_anime.SetTrigger(p_anime);
		
		}

		public void BackToGame()
		{
			m_pauseButton.enabled = true;
			NotificationCenter.DefaultCenter.PostNotification(this, "showButton");
			GameMaster.Instance.GameFSM.ChangeState(GameStateSetUp.Instance);
		}
	}
}